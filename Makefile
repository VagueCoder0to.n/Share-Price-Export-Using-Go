#!make

GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)

gitlab_push:
	- git add *
ifdef c
	- @echo ${c}
	- git commit -m "${c}"
else
	- git commit -m "Config Correction"
endif
	- git push -u origin ${GIT_BRANCH}
